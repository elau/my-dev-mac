brew install git openssl 
brew install awscli heroku
brew install jenv 
brew install pyenv python@2 python
brew install nodenv node node-build
brew install go glide
brew install yarn
brew install postgresql
brew install jq

# If iOS development is required
# brew install cocoapods

# If Android development is required
# brew cask install android-studio
